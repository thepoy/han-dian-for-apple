# 汉典 For Apple 

#### 介绍
macOS/iOS能用的汉典

#### 当前问题：
- 不能用拼音搜索

#### TODO
- 增加拼音索引
- 将所有词条的解释和例句分开
- 所有例句用苹方细体显示
- 根据反馈调其他样式

#### 示例图

![添加汉典](https://images.gitee.com/uploads/images/2021/0323/192726_8c20a3c0_5111352.png "截屏2021-03-23 13.53.22.png")

![输入图片说明](https://images.gitee.com/uploads/images/2021/0323/193051_59c60f78_5111352.png "截屏2021-03-23 11.16.54.png")

![输入图片说明](https://images.gitee.com/uploads/images/2021/0323/193129_65ec8f7f_5111352.png "截屏2021-03-23 11.16.17.png")

![输入图片说明](https://images.gitee.com/uploads/images/2021/0323/193235_410570e2_5111352.png "截屏2021-03-22 18.47.58.png")

![输入图片说明](https://images.gitee.com/uploads/images/2021/0323/193358_ace77bf6_5111352.png "截屏2021-03-22 18.35.31.png")